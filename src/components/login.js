import React,{Component} from 'react';

import {Link,Redirect} from 'react-router-dom';

class Login extends Component{
    state = {
        username : '',
        password : '',
        elementsDisabled : false,
        animation : '',
        error : ''
    }
    usernameCheck = (response) => {
        // console.log(response)
        if (response.code) {
            this.setState({
                error: response.code.split('/')[1] + " : " + response.message,
                elementsDisabled: false,
                animation: ""
            });
            return;
        } else {
            const data = {
                password: this.state.password
            }
            this.setState({
                usernameerror: '',
                email: response.email
            })
            data.email = response.email

            if (!this.state.error)
                this.props.middleware.signIn(data.email, data.password, this.callback);
        }
    }
    signIn = (e) =>{
        e.preventDefault();
        this.setState({
            elementsDisabled : true,
            animation : "pulse"
        });
        const mailExp = /^.+@.+\..+/
        const phoneExp = /^\+[0-9]{1,3}[0-9]{10}/
        if (this.state.username.match(mailExp)) {
            this.props.middleware.getUserByEmail(this.state.username.toLowerCase(), this.usernameCheck);
        } else if (this.state.username.match(phoneExp)) {
            this.props.middleware.getUserByPhoneNumber(this.state.username.toLowerCase(), this.usernameCheck);
        } else
            this.props.middleware.getUserById(this.state.username.toLowerCase(), this.usernameCheck);
    }
    callback = (response) =>{
        console.log(response)
        if(this.__componentMount)
        if(response.code)
            this.setState({
                error: response.code.split('/')[1] +" : "+ response.message,
                elementsDisabled : false,
                animation : ""
            })
        else
            this.setState({ error: <Redirect to={"/"+ response.next} />})
    }
    componentDidMount = () => {
        this.__componentMount = true;
    }
    componentWillUnmount = () =>{
        this.__componentMount = false;
    }
    update = (e) => {
        this.setState({
            [e.target.id] : e.target.value,
            elementsDisabled: false,
            animation: ""
        });
    }
    render(){
        return(
            <div className="container">
                <div className="card">
                    <div className="row container">
                        <div className="card red">
                            <div className="container" align="center">
                                <h6 className="white-text text-darken-2">{this.state.error}</h6>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col l2 s1"></div>
                        <form className="col s10 l8" onSubmit={this.signIn}>
                            <div className="row">
                                <label htmlFor="username">USERNAME</label>
                                <input onChange={this.update} className="validate" required disabled={this.state.elementsDisabled} placeholder="EMAIL / MOBILE /  USERNAME" id="username" type="text"/>
                            </div>
                            <div className="row">
                                <label htmlFor="password">PASSWORD</label>
                                <input onChange={this.update} className="validate" required disabled={this.state.elementsDisabled} placeholder="********" type="password" id="password"/>
                            </div>
                            <div className="row" align="center">
                                <div className="col s2">
                                </div>
                                <div className="col s4">
                                    <button className={"waves-effect waves-light btn "+this.state.animation} disabled={this.state.elementsDisabled} onClick={this.signIn}>LOGIN</button>
                                </div>
                                <div className="col s4">
                                    <button className={"waves-effect waves-light btn "+this.state.animation} disabled={this.state.elementsDisabled} type="reset">RESET</button>
                                </div>
                            </div>
                            <div className="row" align="center">
                                <Link to='/resetpassword'><u>Forgot your password?</u></Link>
                            </div>
                            <div className="row" align="center">
                                New here? 
                                <Link to='/register'> <u>Join Now</u></Link>
                            </div>
                        </form>
                        <div className="col l2 s1"></div>
                    </div>
                    <div className="row card" align="center">
                        <div className="col s3">
                            <Link to="#signin-google" onClick={() => {this.props.middleware.signInGoogle(this.callback)}}>
                                <img width="50px" height="50px" alt="google" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABUFBMVEX////qQzU0qFNChfT7vAUxffTQ4PI4gPSdu/j7ugCxyPrqQDH7uAD/vQAwp1AaokPpOSnpLhrqPS7pMyH86egnpUoXokLpNyZDg/zpMR78wgAzqkPy+fTzoJr4yMX7393I5M5SsmrymZP3vrvd7+HrSDrsWk+938XtYVfm8+n+6sF8woz97+7xjYaf0aqt2LeQy5792ozoJw5Rkuj80XG50fA7q1n81X///PP8yVHpNzdBhvBtvIBJi+/rUUX62df1sq7vdm7m7/Y8lbX+8tjS6thAieQUp1iztTRKsGQ1pWNvoe04noljuXh7r0beuh794KCtyfDKtBF+q+5WrE/uuxXBty/7xDY/jNmLsUM+kMo6maI3onfI2/E7mKg3oH08k7s5nJU2pGz4pw3uZjnyhzT2nivwdjj1ljD95bLwgnrk15absjz+8dKTuOz93pvVJflkAAAK5klEQVR4nO2baXvbxhGAIYoyI4MECMIAS4n3IVLmaTlh7JiWSNeyy7pq49pu0iM9kjSN1Sb5/9+Kg5R4YBezC+wuoIfvdxt4NbMzs4ulJO3YsWPHjh07doRE76LcmJbqfZt6qTRtlMsXD3qi3yoUKo3Sdec8qxV0PZ/PLsnndb2g5VudWalREf2K1PTK0+vWWUHP5mR5zxtZzmX1wlnrenoh+m1J6ZX753ohn0OpbYjm8oV8pxQfy4vSiaZnYXIrmtmCPpvGYGle1PcKxHZLrFh2pg9EK+Co1Fsatd4ilHmtE9VI9qbnQfVuJGcRXJOVaz0fhp5LTjufijZap3yi5ULTc7AC2Y9OsjbOC+GF75as3o9G1Wm0dBZ+juPZtXjHxjkzP8exIDhXL06Y5Oeao1YS59e71lj72eRbZUGCUz3Lwc9C1mYiUvXiXOfjZ5PTptwF61wS9Bb9hO8+stLKc/WzkM94VpwS5wC66B1eq/FBh+MKXCWnNbgIlvMhj6AEaH0OgqUzYX4W+RPmY9xMUIYuyels23/vnFOTx3A2ZShYEbgEb2G4GMvMx2wQssZqLU6F1pgbZJmZoCbazUHOshIsRUQwx0qwfucFC6LdHNgJ3vkUvfNFpnHX28RFRCKYZyX4ICKCzNag1IrGqMZsDUod8buJPaYRrAveD7owFCxHoowyFOyFeGgoy7nc4jpNDnhJg72gdBLKjte+NaOd7XVm/Xq9VCrV+9fXnbx70wb0z9kVGakfQggtudasVN56yV6lXJrtaf6nBiwjGLjVy1kt19+WW6EynWn42ynsGr0F2WrZIqudlCAfGy76cgEZSZYRlPpBOqGs5+vwdyvPEBdVWK7BQDma004ID+B7JdnjcznTCAaY1nJ0N36me5vTBdMISiXaOiprHdobTdPs2sJgK0i9owj21X31wyvbFJVmdL1eDnpzonKyTFW2EZTKdCHUQ/g0NHXDyDiC0jlNmQnpQ3TlPG93G7aCU5o9U7YV1mUCazUyjqC0RxHCwiy85zcKjAVpOoXIy1nkUFz1PeNziSAkfvmuRegnF0RdPaMjmUx+/wWRoB7Be9kYPqaTyaN/EyjKesx+2ZM8tIJ49AM8UwsxE3xohdBWTEMzVYtXikrS08Oky9F/QIqcrmKFx8t0csnRDwBFvS76jUl5dJi8VUy3/ByzIU4ynEiu4Zepckv0+xLzML2h+CNW8SxmZdTi08PkhuJ3mEzVYzWMOjzZFEziBpxcR/T7kvMx7WGIHHCYXTBjyFOvGNqZ6iWYj1+OSk+8QojK1BjW0e1Kis1ULV47JpfPPZPUVdwcxeNYZhbbCpTixigeu4Hb5iUySbczNRe/cc3iS7yhlakxDyGiV6xl6nLAkWO5CiUfP8dxMYoz/tkDI3yW4ULRHcWzol+WCs+RbVvRHnCyMRxnJGw3XOf7L+I4kVp8BTU8+jGedUYCJanLl9QPeXGPLS8wzwYVGpf0S2rD+wdseYx5Nnrs3uQwSS0o3U/tMyWFefYvcMPPo2t4gEnTrSMadJI+jLDhZ+hng0tpMv0kuoapn9HPxm2d1pP0K3pB9oZvkI/2PGbzNvw0wob7z9GG4EKTpu+GHAwPkI/+CW74U6QNkcUU3g6DFBoOht+gHg3bWTiGAQTZG6beoh4NbvhBJhoehsh28QjcLJ5G2/Ad6tHg3WGgZsHBENkQwUNb1A2Ruwu44aNIG+6/uvOGyKHmzhg+Qz0aXmkibrgf3DDA/ndnGAYhZGm0uwXa8BF4aou4IbKWwufSaE9t6H4I3lsEOsQQOdPAd0+H0TZEzqV3ZQeM3lsQnGLQn+nzMPwE9WiCk6gAB8Iid8AEp4lBxjaBpxgEJ8JB2gX7kyj0sT74VD/QQQ17w3vIZxN8mYnwdwvcxyfwUVQy/THKhuhnE7T8AJMpc0Pk4E30lTvAVMPcEPOZG94QA33HZ/11DTnSSATtIpn+b3QN72MeDi2mmcyf5/SGBykqoIa4r9zQHWLmw/vXZpPW8JtPqHgHVcTdVADuLjJ/TCQSxojWkJLPDqCGRcz/Aik1mcz/XidsRW5uLuAYInf4Dv5zWyb53hFMGG1OagueAwWxpRRw3Jb5Q2KBQl9raHgBTVLMzsLGb6rJ/Ob10jChVvm4ubwFFxr03G2DX4iZ5O9vBRNKjZOcwxugIPZamw1uId5m6CKIl1zcHMBJipvZHDDbi9UM5R5EcJLi7nw5IE+jrDFmQ9AKIr9yCq2kmKsmS1CCH95vCSaUMQ85m3vgJMX2ewfvfpH507Yfz8HmDXgq9VuG3oObV4a6mKcc9EjqDHZjsQCYoVyLDTyE2LF7wVaaOoM2CmPA3o9gFWJuXt6yUU2XgzYK+l0UnMdgQd9e4bC20V8fY7zydOxbvIIC3jf5jmwLVn+DuDnGeOXpFWPBItgPe8y2wspsujXGeKEybhnwMoP56rTO8rQmk/HJ0OVSHLIUJMhRYJLeHJtaGQoStBSr7AThrXAfVkkdnA0GYozxVmS3ywAPpPuwdu9izTXoMYavIsEihMykN3zAjDHeMNorviPJUcxPSbb4iBtjEIpVBoI/kwiC64zDa4VYkUFFJYogQZ2xaavEhgkz7L74hkwQe5q/zZg8iGFPN4/JBIHzzA00QUwY4/DG8BfPCT9R+ZyTbjOnCGJCMcPaTL0lDOA++tosikuTwtAqqbVQdv2ES5AmhJI0MagUwwhj+9mviCNIuAptTmlWoo0xDnbI2KyZ469JFQ/IQyhJA7ogWmFUa1Vqv9OuqSSU418TKlKEUKLrGAEdT7uq+3c9/t0+0URK1guXUBabheOcPFcvJ+ZN3hi/fQUPI/pWsA8j2jx1HA1jRNIeTwdjdTVplOO/gBUhZ4je0OepGwdzPoBJNgc109h82PG/gIoHoBM27wcHyFMHxTCNbhvfIpvDrrGt5yh+Cxts6MqMy4C2ZaxgGOZ4Mqiebm1Pi83qcFQzVAO5GJQEpG34f27CUQuWp8s3VQxVNY35pDsaDQaD0ag7uRobpmrJ4f9/SNvwuZrgRzFIsfEQXaIowD/d8T/82kaQHLWpBl2KQfFrG0Q7e09GISzFQCjH/8Qowr5U4KEcwUME0zZS+BtQQKi2iqFiINtGirrXr1L0KXgcUBTvASdYo7ilKXopWhz/3UORas/kifCCmvBsGwfBOuEa7QgoGpv7YuodhSfDCChutg2iE2B/BhFQtPbFt4maehZKGY2aovHtqxvHsAUjoqgYi7YRTiOMomLi+K+2YirwNOpJFMqNpfi3/RSTCNpUVeHTjYWhfM1K0JpuxA9wdmNkeV+wOBa+0zBY3xa8EjykqhPGgtaWWGi9Cf1LsxdVcYtR4XS1vFgTtBjZ1pg1RqaIMJpdXn4WlwnuYVRMzr8l4x1G9Yr5Vd1NmnOOYVRUplc8UQxUTo6KOeEeQJfTCZdUNcZVMX42lzXmw7ih8vjFA4b2mOkYZ6hdQQm6wlBhNuQYZpdbj8cyHDPJVSt+0fCzac/DrjmKYY6i42dz2fX7oEvkp44H4tffJsXhPJwGaYXvqiraBkFzpASVtPRqw+iFb4WqLUmbroqhzofRWn2eOPd/iC0tO2MSBz2XYns0t94YeOPCvo2iXA04/r4/HIqXg0nCVHFXS5z7J6o57w45/FCTEcXmcDCpjQ3Vvh60iqqaqjKudQft+MqtUiw2L9tD+zqUzWAwbFcvT4uRLpk7duzYsWPHjljxfx/PscJ0F8CdAAAAAElFTkSuQmCC"/>
                            </Link>
                        </div>
                        <div className="col s3">
                            <Link to="#signin-facebook" onClick={() => { this.props.middleware.signInFacebook(this.callback) }}>
                                <img width="50px" height="50px" alt="facebook" src="https://www.vasilikiphotography.com/img/s/v-3/p712271735-3.jpg"/>
                            </Link>
                        </div>
                        <div className="col s3">
                            <Link to="#signin-twitter" onClick={() => { this.props.middleware.signInTwitter(this.callback) }}>
                                <img width="50px" height="50px" alt="twitter" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnBVrqoNAHj8wq6OJ8ZDvuuRXnZhK9_5ur9tHZANFSmGSBDgUDqw"/>
                            </Link>
                        </div>
                        <div className="col s3">
                            <Link to="#signin-github" onClick={() => { this.props.middleware.signInGithub(this.callback) }}>
                                <img width="50px" height="50px" alt="github" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAmVBMVEX///8NJjYAGy4AITIJJDQAHS8AHzEAFSoAABsAACAAAB4AFCkAECcACyRtdn0AGCzy8/QABiIAABj19vfZ3N7k5+lxe4PV2duiqa7N0dTz9PXr7e67wMSYn6UjN0W1ur6QmJ5RXmhBUFvEycwYLz5+h464vsGrsbYAABSIkJZaZm9jbnY5SVWgp6wvQU4oOkcAAABVYGpHVl8MwDvDAAAS9UlEQVR4nNVdaWOqvBJWCKsgIot63AVrRdRb//+Pu9pWyyQBJgjo+3w7PYpMMpl9Jp1O0/AG/mx33PeTzekcx91uN16c0k0Sro67mf/Pa/z3m8TUX+77G6KbttVzFFkmpPsNQoisKk7Psk29uznsl/701a9aAaNleIldW5PkX7JyQGRJs934Ei79V7+yAPxx0u3ZjlpMG6BTlexe9+v4X6ByMAuJbil44jJkKpZOth+DV5NQhGB3kFynCnUPKh1XS3bBqwnhw1smsi09Q959K205Wb6fjJ2Etl2JN3OIdMPJq0nKYjBO9aeYk0OkpKfHdzmS/rZrq7WS9wPVJtt3EK7+QdPq3b4/EE1LRi+mb7Ie1nb6uDQqw80rD+Qk0QXZk9wg9hVVT15Fo3+wFBRRsqJZtmvqn0PDkmTFcvVP3TRsS1NkFLmSlbziPA62bqnyI6pj2Y58Sva7ZTTKmtdTP/rYrQ4puX1AlcueIxnbf20TOHZK5IssWe7iEu4mxX7DdLLcrs/2dR2KadSkcUuU/WCSuoX0KZZ56o99rD7z/F2YGsUWA3HT9o6jF1oFAoY4hvk1Fjctp7uDaxRtpWqFDRDDQ1TAoMSx4zCq/OjJdlHAr1dWrf5oPAahnkuf6l7Je9JmnmzPrpRLo95v3JKLFr3cFR6ul3VIPG+W6L28VewtGt7GvZHDQ7Kl7etz64KVmmfqyu6+tp/h/PDGzmFP+1y3MN+lNt+eIHbamIccyfzflM3LRxM/t86xCSW5IU6du1wOlc3TrJkf7EQbPo2yPW/i5w5cJU/scxP7d0d0snnLStyk9p+anrgytNdtZDUz2MUWb2V7p5qjyKOYdwRVY9t8tPrf3uX9trSo1d+IJA6vXC3Fdpya4GJytlFVapQ3y0/OL0jSsb5fKMFOdjgr/Lmr6/ljnbeB6zbjtlOunNNrInHOebhst7eBP9hp7Gkkbi1vsTJYAq1z+2GFIGWFKjFWzz94NWSXzt4+/9wK2NvsWg9Xzz51brAcqtV2wgUxU1kTx3iSUcfsGXS6rwtEBwuNPYtPrfeS9Xat9UvTQgfGuyH6svrjok+GKey2YiV52DNcRT4rq/4RGxB9/mA/jfGQIVGteG6mMW2qkbpU7FOYMepLXVQzj0+0iiVWU46gGFgrWTpVec6BdpeI1UYsD4OJQpPYO4g/Ze7SBLrvQuBNQtAkittvES2y3mcHb2BIJLbg6wUy/YQ3OYN3TGg5L3fFpM2GljJ6k9GYKohoiSptRL6+py2H4TuoCYgP2iWwV/gvR7S5LfLl1jCmZCFx0em3wYI6hFa/yTetjC3FafIZmzYJKU0oXRp90epIqOhND2k0Rya1NMq71CYxiCl/UUepDI+q3yLWq4t28uFTOoOomG+FlJNpt1siIIYlFdjoIeIrEyreo72nlLlja1EMVy5PU8jaclz04anfeBF62U+c4PuqpXqfUjLEKDqEy/+Z5tBOt7tmyJwut6k7NM3/FYUpAornjBLbZECJmWJV/3Wz7Yjs2L31se4IeHBcW7/V8MpX0QfpPVGL40hbKGaUwj0PHstBVEtJ6vQ9JgfVetT6E6mQR75g6UavMM/vQ5+J9AoDIOOsTUEUM34i6AXwsTBBfZRdyHkBrKUibhE7HeByWMVlDwfKAZGNcx0G+vJEF3woxR78EfKpVPBpSlMUy9GOx7aMyO7p2X2MUrZcgMjF34Hyv8hESeCm6MW6ZURZd99Q3Q33+QM/Wu6Oq/32iv38VozJNQWDtcurTdCLzSofvkn+Jk5gntAp0fVjqGwfNH6GWcEwGI33ycIYmq5tWZpzg2bdCmqHxiLZjyeDjOjz9p/8eharxK7aQl8hd0HWcLOVEgUQcvKy39C6P8dxOjr20555qwXmfu5WO2xap/78twZ1tuCv2XWxS5yGaRccGGXN/5gPneYSMXO1JvKLCN1LMFldYtcqa137bl6z3O5mHwV9bmHJz1qUxUPncG2GfB0ARSPplmVgOJnTBxTL7gn1rmm2lVuReIuklbxLBzrtEveA+bBqtDSN7XGy+41BL1vuJQi8EId3wqA5Q7plqxbwRGlTMEuNwjPYRJ5hM4CH1S31Ckdsdrg5GKXZpQ+w4CRmlREwwbryueyJnUmrFJa7fRugCTiGHrQLrPI0wIRObLyYwiXYItZPhNqedMuDT+1yaXmsyIPiVKf5GqpvDVFp7LdJYbmkoXWiQ0VsPBDRIQbCaR+8lba40QD8BmLB/4VMXOKt/KLFPSQG5oUgH9owlwS9ChPlrp/LGrLqQ6nV9o0RMDvhNgXAfESoCnbJGgV9qHKwya45AQnFHWBSC1fZvMvzBOpHcRgjhww364yD6AXfqGMRtCdqdNwbTUHDZpZNByD8r+Z4Vwzy3aeagTuGHco9Iupfsm0GzBMkS3Q6K6aMriFoK+QbfQA2Nf4EJpAZpb79Hd6mtT3cIKsFB4BNM/IJ/F3F5kPXbW3hdROxByfJCpS/WKEPRIaFZFKmmqFJYBvWoOUyvHMjDJoNcUw6K+4ErhnYgqwBUPqPzQIGDSkOAz8exRQuNgt5gUu1A/n+0BfAu3dw/EBXMzQOTHb3ilX2ve67NQIva6DYYcJWDzeNT1Q5wSQbzCDaj5MITiexUDUpaRPjWopRnt29ASZTfv0LoA1xumLWZgTjDpyw+cqu/a9GvHD+VoJL+1uIXfy9xnxlGmf31cXUILYahMq8G6ZuLcq+G/mWwD74k4bRhv2CAHyDKMvPfCOAB/FGDhA0qOK3QQ0zy6qASIiX84BGdG91v5BxMQbgrk17LQsbk2D+ytovvVWHYjmUoElew6TICBnYse8kFPCBUL5hUVqtURAX8XbAR7wJUw+4TjZCXE3aTDpBmAi7ZpL1I252GwjsokRpTv6+DZTl828IQFmX4cE6BqKUP4Epo2kRRaUyDwB1YfqdGbBKMa5TawEoFqiQFMjQXE09EPXEGEZe91WC5rYDiIANKCm5OsHHrO+EsRqCRscIFgMVJdtmPYnryd1n/40J241eJ2iuGhwhTOfZPbtS1M/KDYysepHZ/QOMNgPnTtp2EqDwEWZR9EoKMT4iMLTVA2XSIBrU3p5C8ILypXMC+hHBBC/x7+/AUAiKREjaOQMKEQf57fcQlFCQUyfOPsBEtH2/lkIEk0Er7SxO4UtlqTiFcQc8AJOJnLzK/70B0RJDZW5pChFVJqO2o91ZaAgmo3LT4ntI94u1CZTVRu8hOIcYCqeiw5xrBME0bMO69lhc0nTiV/oWiPeDkmbRWYjqw7f3D2l9CG0aTGrg63U+voIZlwij3qm4XQrDde0CVZQBgm3yprMW9S2oXHmrQL0fCFirCeUfYsKlrZY/Q2CiiTAWKIXQ58eV5rAT3NqCjnk9cIqcFfT5+Z0YNFosu4TAVX+BaGfvSMXaUJnkfXtllxCY4mwqTXE9d+Lx0tf5wLgqCmCRXLUD1I8SpoBswJ172zyIhqmp8RQq5g2qiEoaf+94kc5H6fuOD3wf06PLM1B8MH6NRsSVhUbg2N068FLqYCIQCDTe1QciowYbAHUo30QnEK6oYoAXZYFxTNoJs+/2rf44BSileOOKIagsvvULMOMIsgSQnrPUApA9ErA86LvsC/YvWbgxkrv2NxE5idWHHQk3cmAfNLaMveXy0luBKe7FdjCN/y2cAOMiu1OovtsWYCCHNQBB8ytW4N9S3IM6l3a1fl6LPQOg/H5VA9VLg5ymM2rVdCNI+dCZwtaR5e+7cv5YjnZr9VfIt4LlwHcjFNZ5o0chp+2pffzUR3DkHq4SKF4om5Tyh2m3LXmqxuhBVCAg/7CCjoBNmRbhXExyb2WqF0RDj3wcweaYI/fPPfy9KuxU5kYINPDDYUEzQmazYHMeVl90blqxeRKJKzD99gS7SB9/71dpsPzGrOTewuchSwIEwuklGetlCZQIYp7CH0ZqsxJVkkXmUsKW/MzmDwDpuIDbHd6Fc+tEXSC22D0FsNXZyfhJsFndFhtTP+fcj1IPJE3sNqkR4EXgMUPDrXhqHQt/nX8l4hOQ9bXgjQAwZwTcpABI0/LpOzSWcf6Mp6r02eIj/MBUASqsA8d8GeLDu8dn7qC1qlDdCuP7lvlMSk8P4EdrvCAoMp6WF6OmC7qJZqyrjO6jZvDAR3hQdXOGFvnJWZa6p0PBRZz+Vjad0vFlJdTJjkn2lW7kgANziEtJYdjKxHEwrOu7EyJLtlV0m9xkG/fs8iFtedRJdi/eV73291BMAjVjiA2+Hu5yimjF9yv6u8PJcXtiZF6J67nO6bCrfg8RlJacyYKwwsJa0f8/OD9yasS9lHgzQbTqp5ruoq4mVyxb19L+KnpujO0eGN0ya11DF4p0meM2SP+MFwlz55MXzHZhOYlKuJsFz9+wNIWZBs5AvQHkKnYTb5OlH8JKxZTQ3Sb0l7Mqb7RaBcAtJITzUJDQv542zrKONo+NljE3EHkpiktPNdyT/A9axxovKOpDj50/+jJ8SGQV0dyxxTUu9GoYik7NROT3b8G8J9G40mT7INEp7UFCN7mZT9/wQk31zmlUhIOkuho/6PZ3qURpFBrdtF8ysBiBPiyeGOY4ldDqIRr/Yw/FWDaqIsJPWcKG7PNAjXjOzRFSc4RzAune+b4QJaMq1nhTXMzpZgH3pmDE8xqKPpO/sn/jbIZF5ycQsMORFRJ52ME0kZSfLKaK1InE/9jq/jFiF5ghR5EOMKHYEA04ErH4BggqP58jbP5a3tVzvvn2JeIvInP0fBygmCnYwls+GK5GTqvYx8PVlHKvx54KFUtjB+zwMKMvrCjkeGpSoHziq/W/k61KOYJZcLxp4bj/QtDTjrjmTObjVCrC4n88Uwwnu3xfSrDjWySwDdGHJculoyWoewbyxmBmajGInbJhndFWsKK/sqhZUoZT+YhnyhAhCndJYOGRvZhneN+L9gvLEXTzcYWVLKZUKhqRdYmor0hcvf8PiiS5p8vJfj4ez/eHs+5WSGZgKyRoUJf85clGAHosKf+qGWZ4KVG0nmX18m4JKINU7RZe2nspETM/8KhN5A+Do6/oeRa4+msadM1Lno1CYUadXWLwJPmq3qb1ShT69NXAuHvXGPl7tVx4WrHegWZVKGQuLEa70szEQI1nBwW15n+rUHihUpcChhFzh6XFkwNRnRmnChQmVKun0HW+TDWQueJ8ambVl3ASpzCk39EWUqlMkIx7I3ck15YbFaZwRQeBJIEKiw7vPmBuWWaQ1jXFVJRC2rrsyrKg7c4MYCX8WMrcqSelJkjhmBZzwnc6Zxz5xzN0LonTbdeuQW+IUThmmstyPJxCJHQjZc4udqbji2lnTVGiSpYheEKFKDwyvidXn5XiRFfK8M/iN5G77UkfmsYVpv5pp+FxIjgvS4TCFVPfomBHmlOvzdRyE31V8PmBP4miyej3wAtOjxSgMGRC6eqi4iWaI3a2gI52chqj8MC0CRCl8n2+ETst2D4gU30NUTi9sEMrPp+4PXPHGmZaiuOIZij0Y+axhGuMoDFmNToyOdoIhR86e/tjeWCmGHM2KEiKKxUapHDPcWeMCooQYsVmkAjmMNZP4fTCcbr1ihEsQCJn4ZxuKafWTuEHpwSSGDUQeGVUjnUtlz66Zgq90GTfglSx1XgY81KddlqshOqlMFrwJhvptV37vvvk+A9q8TWedVLohbziTvJZx+3Dv4gk3i9YcYGqrZHCpcObpqIqdV6T3RnFPENa1g+5bmdtFI7WnBN4NbYXlU01PqYn7ogvR97nKI6aKJyG/CpyLTdxWR0HbsCCWDI/a1QLhd5K44YQiPtMzjgXc/bC7O9fs+MxZx9roNCbS/zcgYyyqiogL7RG3MWYycEdxChkq5im8zgnN6LItcqYLIJNTlelbMV76lwI9rPTo2eCLcmhj9jpc4WoxdjTV9c/fldzD1lLbibaCW1mvx0lRl6KVcZe/VQVfOPim0ZFPx3vqys+OUN+pH+C+cLMDfL0Fo1x6B2DMD9dIVvaehn8G/grOqCMgHReXhk92K17+ckCovdrKbctQVQQBCaqpcZdrVJzqWzJcXz9fv7DNanxDfyBF1oFMWBCKo+OLP6qmlP60ggmteUr0CBuWrURoxrGNeUrsPRpUm2eEhb/tm5ro1qJZGxrqHYXhn+w2pk5IFnJU+WnT2CS6M3P/1D1pN0DSNG4HjbKq0QZXl5J3w3+ge/g1EKfph1q9nMrIdjX3yN7g2yTbZM2tggG41QXLUYsAXH09NiGhYbGKLTt2q5nIYrthu/AnhQ+ErkOIq/kycny+Xa9RhAsD2quY4cjz3GdZFd/kKlGDGbbrm5V0SCEKJbe3c7e6vDlwB8ncc92BJxgojp2L06OrzJdqsBfhpfYtbWyhmciS5rtxpdw+V+i7o6p/7Hqb7q6aVs9R1Llu/N363pXJadn2abZ3fT3H/5bH7xSeP/8aHfch8kmPcdxtxvHi1O6ScLVcTfzveZl5v8BFyhZTHaWoAUAAAAASUVORK5CYII="/>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;