import React,{Component} from 'react';
import {Redirect} from 'react-router-dom';

class ResetLink extends Component{
    state = {
        uid : ''
    }
    componentDidMount(){
        if (!this.props.email) {
            this.setState({
                error: <Redirect to="/resetpassword" />
            })
            return;
        }
        this.setState({
            uid : this.props.email
        })
        localStorage.removeItem('dwauth_email_password_reset')
    }
    goback = () => {
        this.setState({
            uid: <Redirect to='/resetpassword' />
        })
    }
    resetPassword = (e) => {
        e.preventDefault();
        this.props.middleware.resetPassword(this.state.uid, this.callback);
    }
    callback = (action) => {
        if (!action) {
            this.setState({message : "Mail has been sucessfully resent"})
            console.log(this.state)
        }else if(action.code){
            this.setState({
                error : action.code.split('/')[1]+" : "+action.message
            })
        }
    }
    render(){
        return (
            <div className="container">
                <div className="row container">
                    <div className="card red">
                        <div className="container" align="center">
                            <h6 className="white-text text-darken-2">{this.state.error}</h6>
                        </div>
                    </div>
                </div>
                <div className="card row hoverable black darken-4">
                    <div className="card-panel black darken-4" align="center">
                        <div className="card-title white-text">
                            <h3 className="row">
                                RESET PASSWORD
                            </h3>
                            <span className="cyan-text accent-1 row" align="center">
                                <h5>Hello {this.state.uid},</h5>
                            </span>
                        </div>
                    </div>
                    <div className="card-content container white-text accent-1">
                        You've requested to resest the password of "{this.state.uid}", which has been successful. Now you can check your mail which contains a link to change the existing password.
                        If the above mentioned email isn't yours navigate back and request for another link.
                        If you haven't recieved the mail from us wait for a while and then click on "RESEND LINK" button below.
                        If you have no more access to the email mentioned there is nothing we can do, except you can create a new account with new email address.
                        <br />{this.state.message}
                    </div>
                    <div className="card-action white">
                        <button className="btn hoverable cyan" onClick={this.resetPassword}>RESEND LINK</button>
                        <button className="btn hoverable cyan right" onClick={this.goback}>BACK</button>
                        
                    </div>
                </div>
            </div>
        )
    }
}

export default ResetLink;