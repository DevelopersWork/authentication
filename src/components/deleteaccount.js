import React,{Component} from 'react';
import {Link} from 'react-router-dom';

class DeleteAccount extends Component{
    state = {
        error : 'ARE YOU SURE TO DELETE THE ACCOUNT'
    }
    deleteAccount = (e) => {
        e.preventDefault();
        if(!this.state.feedback)
            return;
        this.setState({
            elementsDisabled: true,
            animation: "pulse"
        })

        this.props.middleware.deleteAccount(this.state.feedback,this.callback);
    }
    callback = (response) =>{
        if(response.code)
            this.setState({
                error: response.code.split('/')[1] +" : "+ response.message,
                elementsDisabled : false,
                animation : ""
            })
    }
    update = (e) => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    render(){
        return(
            <div className="container">
                <div className="card">
                    <div className="row container">
                        <div className="card red">
                            <div className="container" align="center">
                                <h6 className="white-text text-darken-2">{this.state.error}</h6>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col l2 s1"></div>
                        <form className="col s10 l8" onSubmit={this.deleteAccount}>
                            <div className="row">
                                <label htmlFor="feedback">FEEDBACK</label>
                                <textarea onChange={this.update} className="validate" required disabled={this.state.elementsDisabled} placeholder="Please enter the reason for closing your account" id="feedback" type="text" />
                            </div>
                            <div className="row" align="center">
                                <div className="col s2">
                                </div>
                                <div className="col s4">
                                    <button className={"waves-effect waves-light btn " + this.state.animation} disabled={this.state.elementsDisabled} onClick={this.deleteAccount}>DELETE</button>
                                </div>
                                <div className="col s4">
                                    <Link to="/profile" className={"waves-effect waves-light btn " + this.state.animation} disabled={this.state.elementsDisabled}>CANCLE</Link>
                                </div>
                            </div>
                        </form>
                        <div className="col l2 s1"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DeleteAccount;