import React,{Component} from 'react';

import {Link,Redirect} from 'react-router-dom';

class ChangePassword extends Component{
    state = {
        displayName: '',
        username: '',
        email: '',
        phoneNumber: '',
        password: '',
        cpassword: '',
        submitDisabled: true,
        error: ''
    }

    update = (e) => {
        this.setState({
            [e.target.id]: e.target.value,
            submitDisabled: true
        });
        const mailExp = /^.+@.+\..+/
        const phoneExp = /^\+[0-9]+/
        if (e.target.id === 'username') {
            this.usernameCheck = (response) => {
                // console.log(response)
                if (response.code) {
                    this.setState({
                        usernameerror: response.code.split('/')[1]+ " : " +response.message,
                        submitDisabled: true,
                        email : ''
                    });
                } else {
                    this.setState({
                        usernameerror: '',
                        submitDisabled: false,
                        email : response.email
                    })
                }
            }
            if (e.target.value.match(mailExp)) {
                this.props.fetchDetails.byEmail(e.target.value.toLowerCase(), this.usernameCheck);
            } else if (e.target.value.match(phoneExp)) {
                this.props.fetchDetails.byPhoneNumber(e.target.value.toLowerCase(), this.usernameCheck);
            } else
                this.props.fetchDetails.byId(e.target.value.toLowerCase(), this.usernameCheck);
        }

    }
    resetPassword = (e) => {
        e.preventDefault();

        if (this.state.submitDisabled || this.state.usernameerror)
            return;
        this.setState({
            submitDisabled: true,
            error: ''
        });
        this.props.resetPassword(this.state.email, this.callback);
    }
    callback = (action) => {
        if(!action){
            localStorage.setItem('uid', this.state.email)
            this.setState({ error: <Redirect to="/resetLink" /> })
            return;
        }
        console.log(action);
        if (action.code)
            this.setState({ error: action.code.split('/')[1] + " : " + action.message });
        else{
            localStorage.setItem('uid',this.state.email)
            this.setState({ error: <Redirect to="/resetlink" /> })
        }
    }
    render() {
        return (
            <div className="container">
                <div className="card">
                    <div className="row container">
                        <div className="card red">
                            <div className="container" align="center">
                                <h6 className="white-text text-darken-2">{this.state.error}</h6>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s1 l2"></div>
                        <form className="col s10 l8" onSubmit={this.signUp}>
                            <div className="row">
                                <label htmlFor="username">Username *</label>
                                <input onChange={this.update} className="validate" required placeholder="EMAIL / MOBILE NO. / UID" id="username" type="text" />
                            </div>
                            <div className="card red">
                                <div className="container-fluid" align="center">
                                    <label className="white-text">{this.state.usernameerror}</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s4">
                                    <button className="waves-effect waves-light btn" disabled={this.state.submitDisabled} id="submit_btn" onClick={this.resetPassword}>RESET</button>
                                </div>
                                <div className="col s4">
                                    <Link to="/login" className="btn">BACK</Link>
                                </div>
                            </div>
                        </form>
                        <div className="col s1 l2"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ChangePassword;