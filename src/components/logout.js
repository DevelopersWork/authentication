import React,{Component} from 'react';

class Logout extends Component{
    state = {
        error : 'PLEASE WAIT...'
    }
    componentDidMount = () => {
        this.props.middleware.signOut(this.callback);
    }
    callback = (response) =>{
        if(response.error)
            this.setState({
                error: response.code.split('/')[1] +" : "+ response.message,
                elementsDisabled : false,
                animation : ""
            })
    }
    render(){
        return(
            <div className="container">
                <div className="card-panel red accent-4 pulse" align="center">
                    <span className="white-text text-darken-2">
                        {this.state.error}
                    </span>
                </div>
            </div>    
        );
    }
}

export default Logout;