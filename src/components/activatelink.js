import React,{Component} from 'react';

class ActivateLink extends Component{
    state = {
        uid: '',
        user : {
            displayName : "Developers@Work",
            email : "no-reply@developers.work"
        }
    }
    componentWillMount() {
        // this.setState({
        //     uid: localStorage.getItem('uid')
        // })
        // if (!localStorage.getItem('uid')) {
        //     this.setState({
        //         uid: <Redirect to='/login' />
        //     })
        // }
        // console.log(this.props)
    }
    componentDidMount(){
        this.setState({
            user : this.props.middleware.state.auth.currentUser
        })
    }
    goback = () => {
        this.props.middleware.signOut(this.callback);
    }
    activateAccount = (e) => {
        e.preventDefault();
        this.props.middleware.verifyEmail(this.callback);
    }
    callback = (action) => {
        if (!action) {
            this.setState({ message: "Mail has been sucessfully resent" })
            console.log(this.state)
        } else if (action.code) {
            this.setState({
                error: action.code.split('/')[1] + " : " + action.message
            })
        }
    }
    render() {
        return (
            <div className="container">
                <div className="row container">
                    <div className="card red">
                        <div className="container" align="center">
                            <h6 className="white-text text-darken-2">{this.state.error}</h6>
                        </div>
                    </div>
                </div>
                <div className="card row hoverable black darken-4">
                    <div className="card-panel black darken-4" align="center">
                        <div className="card-title white-text">
                            <h3 className="row">
                                EMAIL VERIFICATION
                            </h3>
                            <span className="cyan-text accent-1 row" align="center">
                                <h5>Hello {this.state.user.displayName},</h5>
                            </span>
                        </div>
                    </div>
                    <div className="card-content container white-text accent-1">
                        You've have sucessfully created an account using the email-id "{this.state.user.email}", but the account haven't got activated.
                        To activate your account all you have to do is just open up your email and click on the link we've sent you. If you haven't 
                        recieved any mail then please click on the "RESEND LINK" button below. Once your email gets verified you can login with your email and password.
                        <br />{this.state.message}
                    </div>
                    <div className="card-action white">
                        <button className="btn hoverable cyan" onClick={this.activateAccount}>RESEND LINK</button>
                        <button className="btn hoverable cyan right" onClick={this.goback}>LOGIN</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ActivateLink;