import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/storage';
import axios from 'axios';

import Login from './signin/';
import Link from './link/';

class Firebase{
    state = {
        monthNames : {
            "Jan":"01","Feb":"02","Mar":"03","Apr":"04","May":"05","Jun":"06",
            "Jul":"07","Aug":"08","Sep":"09","Oct":"10","Nov":"11","Dec":"12"
        },
        firebase : firebase,
        firebaseConfig : null,
        auth : null,
        store : null,
        database : null,
        storage : null,
        error : true
    }
    action = {
        error : false,
        errormsg : '',
        next : ''
    }
    resetAction = () => {
        this.action.error = false;
        this.action.errormsg = '';
        this.action.next = '';
    }
    
    firebaseUrl = 'http://localhost:5001/authenticate-developerswork/us-central1';

    initializeApp = (reportBack) => {
        const config = JSON.parse(localStorage.getItem('firebaseDWAuthConfig'));
        // console.log(config)
        this.state.firebase.initializeApp(config);
        this.state.auth = firebase.auth();
        this.state.store = firebase.firestore();
        this.state.database = firebase.database();
        this.state.storage = firebase.storage();
        this.state.error = false;
        this.state.auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
        this.state.auth.onAuthStateChanged(user => {
            if (user) {
                reportBack({
                    error : false,
                    firebaseLoaded : true,
                    userLoggedIn: true,
                    CheckingLogin: false
                })
            }else{
                reportBack({
                    error: false,
                    firebaseLoaded: true,
                    userLoggedIn: false,
                    CheckingLogin: false
                })
            }
        });
        // console.log(this.action)
        reportBack({
            error: false,
            firebaseLoaded: true,
            CheckingLogin: true
        });
    }

    constructor(reportBack){
        if(localStorage.getItem('firebaseDWAuthConfig')){
            this.initializeApp(reportBack);
        }
        else axios.post(this.firebaseUrl+'/getFirebaseConfig',{password : 'hIYA bitch :)'})
        .then(res => {
            localStorage.setItem('firebaseDWAuthConfig',JSON.stringify(res.data));
            this.initializeApp(reportBack);
        }).catch(err => {
            reportBack({
                error: true,
                firebaseLoaded: false,
                userLoggedIn: false,
                CheckingLogin: false,
                message : err
            });
        })
    }
    signUp = (parameters,callback) => {
        const data = {
            uid : parameters.username,
            password : parameters.password,
            email : parameters.email,
            phoneNumber : parameters.phoneNumber,
            displayName : parameters.displayName
        }
        console.log(data)
        axios.post(this.firebaseUrl+'/createUser',data)
        .then(res => {
            res.data.next = "login";
            callback(res.data);
        }).catch(err => {
            err.error = true;
            callback(err);
        })
    }
    signIn = (email,password,callback) => {
        if(this.state.error)
            return this.action;
        this.state.auth.signInWithEmailAndPassword(email,password)
        .then(res => {
            if(res.user.emailVerified){
                res.user.next = "profile";
                callback(res.user);
            }else{
                this.verifyEmail(res.email,callback);
            }
        })
        .catch(err => {
            err.error = true;
            callback(err);
        })

    }
    signOut = (reportBack) => {
        this.state.auth.signOut().then(() => {
            reportBack({
                error: false,
                firebaseLoaded: true,
                userLoggedIn: false,
                CheckingLogin: false
            })
        }).catch(err => {
            reportBack({
                error: true,
                firebaseLoaded: true,
                userLoggedIn: false,
                CheckingLogin: false,
                message: err
            });
        })
    }
    signInGoogle = (callback) => {
        new Login().googleSignIn(new this.state.firebase.auth.GoogleAuthProvider(),this.state.auth,callback);
    }
    signInFacebook = (callback) => {
        new Login().facebookSignIn(new this.state.firebase.auth.FacebookAuthProvider(), this.state.auth, callback);
    }
    signInTwitter = (callback) => {
        new Login().twitterSignIn(new this.state.firebase.auth.TwitterAuthProvider(), this.state.auth, callback);
    }
    signInGithub = (callback) => {
        new Login().githubSignIn(new this.state.firebase.auth.GithubAuthProvider(), this.state.auth, callback);
    }
    linkGoogle = (callback) => {
        new Link().providerLink(new this.state.firebase.auth.GoogleAuthProvider(), this.state.auth, callback);
    }
    linkFacebook = (callback) => {
        new Link().providerLink(new this.state.firebase.auth.FacebookAuthProvider(), this.state.auth, callback);
    }
    linkTwitter = (callback) => {
        new Link().providerLink(new this.state.firebase.auth.TwitterAuthProvider(), this.state.auth, callback);
    }
    linkGithub = (callback) => {
        new Link().providerLink(new this.state.firebase.auth.GithubAuthProvider(), this.state.auth, callback);
    }
    unLinkGoogle = (callback) => {
        new Link().providerUnLink("google.com", this.state.auth, callback);
    }
    unLinkFacebook = (callback) => {
        new Link().providerUnLink("facebook.com", this.state.auth, callback);
    }
    unLinkTwitter = (callback) => {
        new Link().providerUnLink("twitter.com", this.state.auth, callback);
    }
    unLinkGithub = (callback) => {
        new Link().providerUnLink("github.com", this.state.auth, callback);
    }
    resetPassword = (email,callback) => {
        this.state.auth.sendPasswordResetEmail(email)
        .then(res => {
            callback(res);
        }).catch(err => {
            callback(err);
        });
    }
    verifyEmail = (callback) => {
        this.state.auth.currentUser.sendEmailVerification()
        .then(res => {
            if(!res)
                callback({
                    error : false,
                    next: 'activatelink'
                });
        }).catch(err => {
            err.error = true
            callback(err);
        });
    }

    deleteAccount = (feedback,callback) => {
        if(!this.state.auth.currentUser)
            return;
        const time = this.state.auth.currentUser.metadata.creationTime.split(' ')
        
        const data = {
            uid : this.state.auth.currentUser.uid,
            creationTime: {
                year: time[3],
                day: time[1],
                month: this.state.monthNames[time[2]]
            },
            feedback : feedback
        }
        // console.log(data)
        // this.state.auth.signOut();
        axios.post(this.firebaseUrl + '/deleteUser', data)
        .then(res => {
            // callback(res.data);
            this.signOut(callback);
        }).catch(err => {
            err.error = true;
            callback(err);
        })
    }

    getUserById = (uid, callback) => {
        const data = {
            uid: uid
        }
        axios.post(this.firebaseUrl + '/getUserById', data)
        .then(res => {
            callback(res.data);
        }).catch(err => {
            callback({
                error:true,
                code : err.code,
                message : err.message
            });
        })
    }
    getUserByPhoneNumber = (phoneNumber, callback) => {
        const data = {
            phoneNumber : phoneNumber
        }
        axios.post(this.firebaseUrl + '/getUserByPhoneNumber', data)
        .then(res => {
            callback(res.data);
        }).catch(err => {
            callback(err);
        })
    }
    getUserByEmail = (email, callback) => {
        const data = {
            email : email
        }
        axios.post(this.firebaseUrl + '/getUserByEmail', data)
        .then(res => {
            callback(res.data);
        }).catch(err => {
            callback(err);
        })
    }
}

export default Firebase;