
class Login {

    googleSignIn = (provider,auth,callback) => {
        // const provider = new auth.GoogleAuthProvider();
        auth.signInWithPopup(provider)
        .then(res => {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = res.credential.accessToken;
            // The signed-in user info.
            var user = res.user;
            console.log(user);
            res.user.token = token;
            res.user.next = "profile"
            callback(res.user)
        }).catch(err => {
            console.log(err)
            callback(err)
        });
    }

    githubSignIn = (provider,auth, callback) => {
        // var provider = new auth.GithubAuthProvider();
        auth.signInWithPopup(provider)
            .then(res => {
                // This gives you a GitHub Access Token. You can use it to access the GitHub API.
                var token = res.credential.accessToken;
                // The signed-in user info.
                var user = res.user;
                console.log(user);
                res.user.token = token;
                res.user.next = "profile"
                callback(res.user)
            }).catch(err => {
                console.log(err)
                callback(err)
            });
    }

    twitterSignIn = (provider,auth, callback) => {
        // var provider = new auth.TwitterAuthProvider();
        auth.signInWithPopup(provider)
            .then(res => {
                // This gives you a the Twitter OAuth 1.0 Access Token and Secret.
                // You can use these server side with your app's credentials to access the Twitter API.
                var token = res.credential.accessToken;
                // The signed-in user info.
                var user = res.user;
                console.log(user);
                res.user.token = token;
                res.user.next = "profile"
                callback(res.user)
            }).catch(err => {
                console.log(err)
                callback(err)
            });
    }

    facebookSignIn = (provider,auth, callback) => {
        // var provider = new auth.FacebookAuthProvider();
        auth.signInWithPopup(provider)
            .then(res => {
                // This gives you a Facebook Access Token. You can use it to access the Facebook API.
                var token = res.credential.accessToken;
                // The signed-in user info.
                var user = res.user;
                console.log(user);
                res.user.token = token;
                res.user.next = "profile"
                callback(res.user)
            }).catch(err => {
                console.log(err)
                callback(err)
            });
    }

}

export default Login;