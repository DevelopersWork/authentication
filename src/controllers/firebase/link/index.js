
class Link {

    providerLink = (provider,auth,callback) => {
        // const provider = new auth.GoogleAuthProvider();
        auth.currentUser.linkWithPopup(provider)
        .then(res => {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = res.credential.accessToken;
            // The signed-in user info.
            // var user = res.user;
            // console.log(user);
            res.user.token = token;
            callback(res.user)
        }).catch(err => {
            // console.log(err)
            callback(err)
        });
    }

    providerUnLink = (provider, auth, callback) => {
        // const provider = new auth.GoogleAuthProvider();
        auth.currentUser.unlink(provider).then((res) => {
            // Auth provider unlinked from account
            res = {
                providerId : provider
            }
            // console.log(res)
            callback(res)
        }).catch((err) => {
            // An error happened
            callback(err);
        });
    }
}

export default Link;