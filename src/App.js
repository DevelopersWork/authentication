import React,{Component} from 'react';

import { BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';


import Profile from './components/profile';
import Login from './components/login';
import Register from './components/register';
import ForgotPassword from './components/forgotpassword';
import ActivateLink from './components/activatelink';
import ResetLink from './components/resetlink';
import ChangePassword from './components/changepassword';
import Logout from './components/logout';
import DeleteAccount from './components/deleteaccount';

import Firebase from './controllers/firebase/index';

class App extends Component{
  state = {
    firebaseLoaded : false,
    error : false
  }
  componentDidMount() {
    this.__componentMount = true;
    this.firebase = new Firebase((response) => {
      // console.log(response)
      if (this.__componentMount)
      this.setState(response)
    });
  }
  firebase = null;
  componentWillUnmount(){
    this.__componentMount = false;
  }
  render(){
    if (this.state.error) {
      return (
        <div className="container">
          <div className="card-panel red accent-4">
            <span className="white-text text-darken-2">
              {this.state.error}
            </span>
          </div>
        </div>
      )
    } else if (!this.state.firebaseLoaded || this.state.CheckingLogin){
      return(
        <div className="container">
          <div className="card-panel red accent-4 pulse">
          </div>
        </div>
      )
    } else if (this.state.userLoggedIn) {
      if(!this.firebase.state.auth.currentUser.emailVerified) return(
        <BrowserRouter>
          <Switch>
            <Route path="/logout" component={() => <Logout middleware={this.firebase} />} />
            <Redirect from="/profile/logout" to="/logout" />
            <Redirect from="/profile/signout" to="/logout" />
            <Redirect from="/signout" to="/logout" />

            <Route path="/activate-account" component={() => <ActivateLink middleware={this.firebase} />} />
            <Redirect from="/activatelink" to="activate-account" />
            <Redirect from="/emailVerification" to="activate-account" />
            <Redirect from="/verifyEmail" to="activate-account" />
            <Redirect from='**' to="/activate-account" />
          </Switch>
        </BrowserRouter>
      )
      else 
      return (
        <BrowserRouter>
          <Switch>
            <Route exact path="/profile" component={() => <Profile middleware={this.firebase}/>} />

            <Route path="/profile/deleteaccount" component={() => <DeleteAccount middleware={this.firebase} />} />
            <Redirect from="/profile/delete-account" to="/profile/deleteaccount"/>
            <Redirect from="/profile/deleteAccount" to="/profile/deleteaccount" />

            <Route path="/profile/disableaccount" component={() => <Profile middleware={this.firebase} />} />
            <Redirect from="/profile/disable-account" to="/profile/disableaccount" />
            <Redirect from="/profile/disableAccount" to="/profile/disableaccount" />

            <Route path="/profile/changepassword" component={() => <ChangePassword middleware={this.firebase} />} />
            <Redirect from="/profile/change-password" to="/profile/changepassword" />
            <Redirect from="/profile/changePassword" to="/profile/changepassword" />

            <Route path="/profile/updateprofile" component={() => <Profile middleware={this.firebase} />} />
            <Redirect from="/profile/update-profile" to="/profile/updateprofile" />
            <Redirect from="/profile/updateProfile" to="/profile/updateprofile" />

            <Route path="/logout" component={() => <Logout middleware={this.firebase} />} />
            <Redirect from="/profile/logout" to="/logout" />
            <Redirect from="/profile/signout" to="/logout" />
            <Redirect from="/signout" to="/logout" />
            
            <Redirect from='**' to="/profile"/>
          </Switch>
        </BrowserRouter>
      )
    }
    else
    return (
      <div className="App">
        <div className="error">{this.state.error}</div>
        <BrowserRouter>
          <Switch>
            <Route path="/login" component={() => <Login middleware = {this.firebase}/>} />
            <Redirect from="/signin" to="/login"/>

            <Route path="/register" component={() => <Register middleware={this.firebase}/>} />
            <Redirect from="/signup" to="/register" />
          
            <Route path="/resetpassword" component={() => <ForgotPassword middleware={this.firebase}/>}/>
            <Redirect from="/forgotpassword" to="/resetpassword" />
            
            <Route path="/resetlink" component={() => <ResetLink middleware={this.firebase} email={localStorage.getItem('dwauth_email_password_reset')} />} />
            <Redirect from="/resetLink" to="/resetlink"/>

            <Redirect from='**' to="/login" />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
