const functions = require('firebase-functions');
const cors = require('cors')({origin: true});

const firebaseConfig = {
    apiKey: "AIzaSyD5cFWQJrVo45OPDD8jSNzVS9Jf4p1UGtg",
    authDomain: "authenticate-developerswork.firebaseapp.com",
    databaseURL: "https://authenticate-developerswork.firebaseio.com",
    projectId: "authenticate-developerswork",
    storageBucket: "authenticate-developerswork.appspot.com",
    messagingSenderId: "237631172953",
    appId: "1:237631172953:web:b5e42177e6fd4b32"
};

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const auth = admin.auth();
// const firestore = admin.firestore();
// const database = admin.database();
// const storage = admin.storage();

exports.helloworld = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        response.status(404).send('DOMAIN NOT FOUND');
    });
});

exports.getFirebaseConfig = functions.https.onRequest((request, response) => {
    const conditions = {'hostname':request.hostname,'type':request.method,'parameters':request.body};
    console.log(conditions)
    cors(request, response, () => {
        if(conditions.type !== 'POST'){
            return response.status(500).send('GOOD TRY :)');
        }
        if(conditions.parameters.password !== 'hIYA bitch :)'){
            return response.status(500).send('YOU PLANNED VERY GOOD..');
        }
        return response.status(200).send(firebaseConfig);
    });
});

exports.createUser = functions.https.onRequest((request,response) => {
    const conditions = {'hostname':request.hostname,'type':request.method,'parameters':request.body};
    cors(request, response, () => {
        if(conditions.type !== 'POST'){
            console.log(conditions.parameters)
            response.status(200).send('GOOD TRY :)');
            return ;
        }
        const data = {
            uid : conditions.parameters.uid,
            password : conditions.parameters.password,
            email : conditions.parameters.email,
            phoneNumber : conditions.parameters.phoneNumber,
            displayName : conditions.parameters.displayName,
            photoURL : 'https://www.ibts.org/wp-content/uploads/2017/08/iStock-476085198.jpg'
        }
        auth.createUser(data)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        });
    });
});
exports.intialiseUser = functions.auth.user().onCreate( (user) => {
    const metadate = user.metadata.creationTime.split('-')
    const date = {
        year : metadate[0],
        day : metadate[2].split('T')[0],
        month : metadate[1]
    }
    return firestore.collection("" + user.uid + "_" +date.day+'-'+date.month+'-'+date.year).doc('metadata').set({
        email : user.email,
        creationTime : {
            value : user.metadata.creationTime,
            formattedDate: date
        }
    });
});
exports.authenticateUser = functions.https.onRequest((request, response) => {
    const conditions = { 'hostname': request.hostname, 'type': request.method, 'parameters': request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions.parameters)
            response.status(200).send('GOOD TRY :)');
            return;
        }
        const data = {
            uid : conditions.parameters.uid
        }
        // Revoke all refresh tokens for a specified user for whatever reason.
        // Retrieve the timestamp of the revocation, in seconds since the epoch.
        auth.revokeRefreshTokens(data.uid)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        })
    });
});
exports.updateUser = functions.https.onRequest((request, response) => {
    const conditions = { 'hostname': request.hostname, 'type': request.method, 'parameters': request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions.parameters)
            response.status(200).send('GOOD TRY :)');
            return;
        }
        const data = {
            email: conditions.parameters.email,
            emailVerified : conditions.parameters.emailVerified,
            phoneNumber: conditions.parameters.phoneNumber,
            displayName: conditions.parameters.displayName,
            photoURL: conditions.parameters.phoneURL
        }
        auth.updateUser(conditions.parameters.uid,data)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        });
    });
});
exports.disableUser = functions.https.onRequest((request, response) => {
    const conditions = { 'hostname': request.hostname, 'type': request.method, 'parameters': request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions.parameters)
            response.status(200).send('GOOD TRY :)');
            return;
        }
        const data = {
            disabled : true
        }
        auth.updateUser({ uid: conditions.parameters.uid }, data)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        })
    });
});
exports.deleteUser = functions.https.onRequest((request, response) => {
    const conditions = { 'hostname': request.hostname, 'type': request.method, 'parameters': request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions.parameters)
            response.status(200).send('GOOD TRY :)');
            return;
        }
        const user = {
            uid : conditions.parameters.uid,
            creationTime : conditions.parameters.creationTime
        }
        console.log(user)
        const feedback = conditions.parameters.feedback
        firestore.collection("" + user.uid + "_" + user.creationTime.day + '-' + user.creationTime.month + '-' + user.creationTime.year).doc('metadata').update({
            deleteFeedback: feedback,
            deletionTime: admin.firestore.FieldValue.serverTimestamp()
        }).then(res => {
            // response.status(200).send(res);
            auth.deleteUser(user.uid)
            .then(res => {
                console.log(res)
                response.status(200).send(res);
            }).catch(err => {
                console.log(err)
                response.status(200).send(err);
            })
        })
        .catch(err => {
            response.status(200).send(err);
        })
        
    });
});

exports.getUserById = functions.https.onRequest((request, response) => {
    const conditions = { 'hostname': request.hostname, 'type': request.method, 'parameters': request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions.parameters)
            response.status(200).send('GOOD TRY :)');
            return;
        }
        console.log(conditions.parameters)
        auth.getUser(conditions.parameters.uid)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        })
    });
});
exports.getUserByEmail = functions.https.onRequest((request, response) => {
    const conditions = { 'hostname': request.hostname, 'type': request.method, 'parameters': request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions.parameters)
            response.status(200).send('GOOD TRY :)');
            return;
        }
        auth.getUserByEmail(conditions.parameters.email)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        })
    });
});
exports.getUserByPhoneNumber = functions.https.onRequest((request, response) => {
    const conditions = { 'hostname': request.hostname, 'type': request.method, 'parameters': request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions.parameters)
            response.status(200).send('GOOD TRY :)');
            return;
        }
        auth.getUserByPhoneNumber(conditions.parameters.phoneNumber)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        })
    });
});